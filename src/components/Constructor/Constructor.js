import React from 'react';
import Grid from "@material-ui/core/Grid";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from "@material-ui/core/TextField";
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';


const Constructor = ({state, onChange, ingredient, removeItem, quantity, add, remove}) => {

    return (
        <Grid item>
            <Grid container justify='space-between' alignItems='center'>
                <Grid item>
                    <FormControlLabel
                        control={<Checkbox checked={state[ingredient]} onChange={onChange} name={ingredient} />}
                        label={ingredient}
                    />
                </Grid>
                <Grid item >
                    <div style = {{display: "inline-block"}}>
                        <IconButton onClick={() => remove(ingredient)} aria-label="delete">
                            <RemoveIcon />
                        </IconButton>
                    <TextField
                        variant="outlined"
                        defaultValue="Small"
                        size="small"
                        style = {{width: 40}}
                        value ={quantity}
                    />
                        <IconButton onClick={() => add(ingredient)}  aria-label="delete">
                            <AddIcon />
                        </IconButton>
                    </div>

                    <IconButton onClick={() => removeItem(ingredient)} aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                </Grid>
            </Grid>

        </Grid>

    );
};

export default Constructor;