import React, {useState} from 'react';
import './App.css';
import {nanoid} from 'nanoid';
import {Container} from "@material-ui/core";
import {CssBaseline} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Burger from "./components/Burger/Burger";
import Constructor from './components/Constructor/Constructor';
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";

let INGREDIENTS = [
    {name: 'Meat', price: 50},
    {name: 'Cheese', price: 20 },
    {name: 'Salad', price: 5},
    {name: 'Bacon', price: 30},

];
const useStyles = makeStyles(theme => ({
    root:{
        marginTop: theme.spacing(2),
    }
}));
const App = () => {
    const classes = useStyles();
    const [ingredients, setIngredients] = useState([]);
    const [state, setState] = useState({
            Meat: false,
            Cheese: false,
            Salad: false,
            Bacon: false,
    });

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
        let name = event.target.name;
        const index = INGREDIENTS.findIndex(i => i.name===name);
        if(event.target.checked){
            setIngredients([
                ...ingredients,
                {
                    id: nanoid(),
                    name: name,
                    count: 1,
                    price: INGREDIENTS[index].price
                }]);
        } else {
            const index = ingredients.findIndex(i => i.name===name);
            const ingredientsCopy = [...ingredients];
            ingredientsCopy.splice(index, 1);
            setIngredients(ingredientsCopy);
        }
    };

    const totalPrice =() =>{
        return ingredients.reduce((acc, item)=>{
            return acc + item.price;
        }, 20)
    };

    const quantity = ingredient => {
        return ingredients.reduce((acc, item)=>{
            if(item.name === ingredient){
                return acc +1
            } else {
                return acc
            }
        },0)
    };

    const removeItem = (ingredient) =>{
        const index = ingredients.findIndex(i => i.name===ingredient);
        const ingredientsCopy = [...ingredients];
        ingredientsCopy.splice(index, 1);
        setIngredients(ingredientsCopy);
        let statesCopy = {...state};
        if(quantity(ingredient)===1){
            statesCopy[ingredient] = false;
            setState(statesCopy);
        }
    }
    const removeAll = (ingredient) =>{
        let ingredientsCopy = [...ingredients];
        ingredientsCopy = ingredientsCopy.filter(item => item.name !== ingredient)
        setIngredients(ingredientsCopy);
        let statesCopy = {...state};
        statesCopy[ingredient] = false;
        setState(statesCopy);

    }
    const addItem = (ingredient) =>{
        const index = INGREDIENTS.findIndex(i => i.name===ingredient);
        setIngredients([
            ...ingredients,
            {
                id: nanoid(),
                name: ingredient,
                count: 1,
                price: INGREDIENTS[index].price
            }]);
        let statesCopy = {...state};
        statesCopy[ingredient] = true;
        setState(statesCopy);
    }

    return (
        <Container maxWidth = 'md' className={classes.root}>
            <CssBaseline/>
            <Grid container spacing = {2} alignItems="center" >
                <Grid item xs={6}>
                    <Paper component={Box} p={2}>
                        <Grid container spacing = {2}  direction='column'>
                            {INGREDIENTS.map((ingredient, index) => {
                                let quant = quantity(ingredient.name);
                                return (<Constructor
                                    key={index}
                                    ingredient={ingredient.name}
                                    add = {addItem}
                                    remove = {removeItem}
                                    quantity={quant}
                                    state = {state}
                                    removeItem={removeAll}
                                    onChange={handleChange}
                                />)
                            })}
                        </Grid>
                    </Paper>
                </Grid>
                <Grid  item xs={6}>
                    <Burger ingredients={ingredients} price = {totalPrice()}/>
                </Grid>
            </Grid>

        </Container>
    );
};

export default App;