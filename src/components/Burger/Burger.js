import React from 'react';
import './Burger.css';
import Paper from "@material-ui/core/Paper";
import Bun from "../Bun/Bun";
import Box from "@material-ui/core/Box";
import Ingredients from "../Ingredients/Ingredients";


const Burger = ({ingredients, price}) => {

    return (
        <Paper component={Box} p={2}>
            <div className="Burger">
                <Bun bun ='top'/>
                {ingredients.map(ingredient => (
                    <Ingredients key={ingredient.id} ingredient ={ingredient.name}/>))
                }
                <Bun bun = 'btm'/>
            </div>
            <p>Price: {price}</p>

        </Paper>
    );
};

export default Burger;