import React from 'react';
import './Bun.css';

const Bun = props => {
    if(props.bun==='top'){
        return (
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
        );
    } else {
        return (
            <div className="BreadBottom"></div>
        );
    }
};

export default Bun;